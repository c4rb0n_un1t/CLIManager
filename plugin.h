#pragma once

#include <QtCore>

#include "../../../Application/Interfaces/iapplication.h"
#include "../../Interfaces/Architecture/iclielement.h"
#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.CLIManager" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

	// PluginBase interface
protected:
	void onReady() override;
	void onReferencesListUpdated(Interface interface) override;

private:
	void processInitialArgs(const QStringList& args);

private slots:
	void readCommand();
	void processCommand(const QStringList& args);

private:
	ReferenceInstancePtr<IApplication> m_application;
	ReferenceInstancesListPtr<ICLIElement> m_elements;

	QMap<QString, ReferenceInstancePtr<ICLIElement>> m_commandToElement;
	QSharedPointer<QSocketNotifier> m_notifier;
	QCommandLineParser m_parser;
};
